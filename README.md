# beeptron

CGI script that renders beeptext file to gemini markup.  
The output should be lace compatible.

## Usage

Upload beeptron to your server and put it in the cgi dir. Create the cgi env var `BEEPTEXT_FILE` with the path relative to the root of your server to your beeptext file.
