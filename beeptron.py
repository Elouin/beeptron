#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
import re
import os

BEEPTEXT_FILE = os.environ["BEEPTEXT_FILE"]

def generate_gmi(beeptext_file: str) -> None:
    beeps = {}
    for line in open(BEEPTEXT_FILE, 'r').readlines():
        if not re.match(r"^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\+[0-9]{2}:[0-9]{2}", line):
            continue
        beeps[datetime.fromisoformat(line.split(" ")[0])] = line.split(" ")[1:]
    print("20\ttext/gemini\r\n")
    print("# %s's beepfeed\n"%BEEPTEXT_FILE.split(".")[0])
    print("You can either follow this directly with lace or:")
    print("=> %s follow this beepfeed\n"%BEEPTEXT_FILE)
    for post_date in sorted(beeps, reverse = True):
        print("## " + post_date.strftime("%a %d %b %Y %H:%M"))
        print(str(" ".join(beeps[post_date])))

generate_gmi(BEEPTEXT_FILE)

